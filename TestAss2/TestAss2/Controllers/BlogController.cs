﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestAss2.Models;

namespace TestAss2.Controllers
{
    public class BlogController : Controller
    {
        BlogClient blClient = new BlogClient();
        UserClient usClient = new UserClient();
        TypeBlogClient typeBL = new TypeBlogClient();
        // GET: Blog
        public ActionResult Index()
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var dataUser = usClient.CheckUser(Session["loginSession"].ToString());
            var email = dataUser.Email;
            var pwd = dataUser.Password;
            var data = blClient.GetAllLisBlog(email,pwd);
            
            ViewBag.DataUser = usClient.getAllUser();

            return View(data);
        }

        // GET: Blog/Details/5
        public ActionResult Details(int id)
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var dataUser = usClient.CheckUser(Session["loginSession"].ToString());
            var email = dataUser.Email;
            var pwd = dataUser.Password;
            var data = blClient.GetBlogById(id,email,pwd);
            foreach(var item2 in usClient.getAllUser())
            {
                if(item2.ID == data.UserID)
                {
                    ViewBag.DataUser = item2.UserName;
                }    
            }
            foreach(var item3 in typeBL.getAllTypeBlog())
            {
                if(item3.ID == data.TypeBlogID)
                {
                    ViewBag.DataTypeBl = item3.TypeName;
                }
            }
            ViewBag.DataDetail = data;
            ViewBag.ContentShort = data.ContentShort;
            if(data.Status==1)
            {
                ViewBag.Status = "Hiện";
            }
            else
            {
                ViewBag.Status = "Ẩn";
            }
            ViewBag.Content = data.Content;
            return View(data);
        }

        // GET: Blog/Create
        public ActionResult Create()
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var dataUser = usClient.CheckUser(Session["loginSession"].ToString());
            ViewBag.UserID = dataUser.ID;
            ViewBag.TypeBlogID = new SelectList(typeBL.getAllTypeBlog(),"ID", "TypeName");
            return View();
        }

        // POST: Blog/Create
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Blog bl,int status)
        {
            ViewBag.TypeBlogID = new SelectList(typeBL.getAllTypeBlog(), "ID", "TypeName", bl.TypeBlogID);
            try
            {
                // TODO: Add insert logic here
                if (Session["loginSession"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }
                var dataUser = usClient.CheckUser(Session["loginSession"].ToString());
                var email = dataUser.Email;
                var pwd = dataUser.Password;
                bl.UserID = dataUser.ID;
                bl.Image = "1";
                bl.Status = status;
                blClient.StoreBlog(bl,email,pwd);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Blog/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var dataUser = usClient.CheckUser(Session["loginSession"].ToString());
            var email = dataUser.Email;
            var pwd = dataUser.Password;
            var data = blClient.GetBlogById(id,email,pwd);
            ViewBag.TypeBlogID = new SelectList(typeBL.getAllTypeBlog(), "ID", "TypeName",data.TypeBlogID);
            return View(data);
        }

        // POST: Blog/Edit/5
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, Blog bl)
        {
            ViewBag.TypeBlogID = new SelectList(typeBL.getAllTypeBlog(), "ID", "TypeName", bl.TypeBlogID);
            try
            {
                // TODO: Add update logic here
                if (Session["loginSession"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }
                var dataUser = usClient.CheckUser(Session["loginSession"].ToString());
                var email = dataUser.Email;
                var pwd = dataUser.Password;
                bl.Image = "1";
                bl.UserID = dataUser.ID;
                blClient.EditBlog(bl);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Blog/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Blog/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
