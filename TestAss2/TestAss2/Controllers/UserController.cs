﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestAss2.Models;

namespace TestAss2.Controllers
{
    public class UserController : Controller
    {
        UserClient usData;
        HomeController homeControl;
        public UserController()
        {
            usData = new UserClient();
            homeControl = new HomeController();
        }
        // GET: User
        public ActionResult Index()
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
           
                var data = usData.getAllUser();
                return View(data);
            
            
        }

        // GET: User/Details/5
        public ActionResult Details(int id)
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var data = usData.getUserById(id);

            return View(data);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        public ActionResult Create(User us)
        {
            try
            {
                // TODO: Add insert logic here
                if (Session["loginSession"] == null)
                {
                    return RedirectToAction("Index", "Home");
                }
                usData.StoreUser(us);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Edit/5
        public ActionResult Edit(int id)
        {
            if (Session["loginSession"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var data = usData.getUserById(id);
            return View(data);
        }

        // POST: User/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, User us)
        {
            try
            {
                
                usData.EditUser1(us);
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: User/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: User/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

       
    }
}
