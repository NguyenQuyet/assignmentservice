﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestAss2.Models;

namespace TestAss2.Controllers
{
    public class HomeController : Controller
    {
        UserClient usserData;
        public HomeController()
        {
            usserData = new UserClient();
        }
        public ActionResult Index()
        {
            if(Session["loginSession"]==null)
            {
                return RedirectToAction("LoginSession");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult LoginSession()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginSession(string email, string pwd)
        {
            var data = usserData.getAllUser();
            foreach(var item in data)
            {
                if (item.Email == email && item.Password == pwd)
                {
                    Session["loginSession"] = email;
                    return RedirectToAction("Index");
                }
               
            }
           /* if(email == "admin@gmail.com" && pwd == "123456")
            {
                Session["loginSession"] = email;
                return RedirectToAction("Index");
            }*/
            return View();
        }

        public ActionResult LogoutSession()
        {
            Session.Remove("loginSession");
            return RedirectToAction("Index");
        }
        
    }
}