﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestAss2.Models;

namespace TestAss2.Controllers
{
    public class TypeBlogController : Controller
    {
        TypeBlogClient typeBlogClient;
        public TypeBlogController()
        {
            typeBlogClient = new TypeBlogClient();
        }
        // GET: TypeBlog
        public ActionResult Index()
        {
            var data = typeBlogClient.getAllTypeBlog();
            return View(data);
        }

        // GET: TypeBlog/Details/5
        public ActionResult Details(int id)
        {
            var data = typeBlogClient.getTypeBlogById(id);
            return View(data);
        }

        // GET: TypeBlog/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: TypeBlog/Create
        [HttpPost]
        public ActionResult Create(TypeBlog tp)
        {
            try
            {
                // TODO: Add insert logic here
                typeBlogClient.AddTypeBlog(tp);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TypeBlog/Edit/5
        public ActionResult Edit(int id)
        {
            var data = typeBlogClient.getTypeBlogById(id);
            return View(data);
        }

        // POST: TypeBlog/Edit/5
        [HttpPost]
        public ActionResult Edit(TypeBlog tp)
        {
            try
            {
                // TODO: Add update logic here
                typeBlogClient.EditTypeBlog(tp);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: TypeBlog/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: TypeBlog/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
