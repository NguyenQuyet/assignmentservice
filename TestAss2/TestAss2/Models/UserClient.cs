﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestAss2.UserServiceReference;

namespace TestAss2.Models
{

    public class UserClient
    {
        UserServiceClient client = new UserServiceClient();
        public List<User> getAllUser()
        {
            var list = client.GetUserList().ToList();
            var rt = new List<User>();
            list.ForEach(a => rt.Add(new User()
            {
                Email = a.Email,
                ID = a.ID,
                Created = a.Created,
                Password = a.Password,
                UserName = a.UserName,
                
            }));

            return rt;
        }
        public User getUserById(int id)
        {
            var data = client.GetUserById(id);
            User u = new User();
            u.Email = data.Email;
            u.ID = data.ID;
            u.UserName = data.UserName;
            u.Password = data.Password;
            return u;
        }
        public User CheckUser(string email)
        {
            var data = client.GetUserCheck(email);
            User u = new User();
            u.Email = data.Email;
            u.ID = data.ID;
            u.UserName = data.UserName;
            u.Password = data.Password;
            return u;
        }
        public void StoreUser(User us)
        {
            var u = new UserServiceReference.User()
            {
                ID = us.ID,
                Email = us.Email,
                Password = us.Password,
                Created = us.Created,
                UserName = us.UserName,
            };
            client.AddUser(u);
        }

        public void EditUser1(User us)
        {
            var u = new UserServiceReference.User()
            {
                ID = us.ID,
                Email = us.Email,
                Password = us.Password,
                Created = us.Created,
                UserName = us.UserName,
            };
            client.UpdateUser(u);
        }

    }
}