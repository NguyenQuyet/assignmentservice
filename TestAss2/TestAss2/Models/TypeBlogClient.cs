﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestAss2.TypeBlogServiceReference;

namespace TestAss2.Models
{
    public class TypeBlogClient
    {
        TypeBlogServiceClient client = new TypeBlogServiceClient();
        public List<TypeBlog> getAllTypeBlog()
        {
            var list = client.GetTypeBlogList();
            var tp = new List<TypeBlog>();
            list.ForEach(t => tp.Add(new TypeBlog
            {
                ID = t.ID,
                TypeName = t.TypeName,
                Descriptions = t.Descriptions,
                Status = t.Status,
               
            })) ;
            return tp;
        }

        public TypeBlog getTypeBlogById(int id)
        {
            var data = client.GetTypeBlogById(id);
            TypeBlog tpb = new TypeBlog();
            tpb.ID = data.ID;
            tpb.TypeName = data.TypeName;
            tpb.Descriptions = data.Descriptions;
            tpb.Status = data.Status;
            return tpb;
        }
        public void AddTypeBlog(TypeBlog tb)
        {
            var newTp = new TypeBlogServiceReference.TypeBlog()
            {
                TypeName = tb.TypeName,
                Status = tb.Status,
                Descriptions = tb.Descriptions,
                
            };
            client.AddTypeBlog(newTp);
        }

        public void EditTypeBlog(TypeBlog tb)
        {
            var type = new TypeBlogServiceReference.TypeBlog()
            {
                ID = tb.ID,
                TypeName = tb.TypeName,
                Status = tb.Status,
                Descriptions = tb.Descriptions,
            };
            client.UpdateTypeBlog(type);
        }
    }
}