﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestAss2.Models
{
    public class Blog
    {
        public int ID { get; set; }
       
        public string Title { get; set; }
        
        [DataType(DataType.MultilineText)]
        public string ContentShort { get; set; }
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        
        public DateTime Created { get; set; }
        
        public int Status { get; set; }
        
        public string Place { get; set; }
        
        public int views { get; set; }
       
        public int UserID { get; set; }
        public string Image { get; set; }
        
        public int TypeBlogID { get; set; }

        [ForeignKey("TypeBlogID")]
        public virtual TypeBlog TypeBlog { get; set; }

        [ForeignKey("UserID")]
        public virtual User User { get; set; }

        /*public ICollection<Comments> Comments { get; set; }*/
    }
}